
const express = require('express');
const app = express();
require('dotenv').config();
const mongoose = require('mongoose');
const cors = require('cors');
const { MONGO_URL, PORT } = process.env;
const fallbackPort = 4000;


const finalPort = PORT || fallbackPort;
const finalMongoURL = MONGO_URL;
mongoose
    .connect("mongodb+srv://yhemanth:Hemanth123@icekart.zpgzf5g.mongodb.net/project", {

    })
    .then(() => console.log("MongoDB is connected successfully"))
    .catch((err) => {
        console.error("Error connecting to MongoDB:", err);
        process.exit(1);
    });

// Middleware
app.use(express.json());
app.use(cors());

// Routes
app.use('/register', require('./credentialroutes/Register'));
app.use('/display', require('./credentialroutes/Display'));
app.use('/data', require('./credentialroutes/Data'));
app.use('/', require('./credentialroutes/getData'));

// Start the server
app.listen(finalPort, () => {
    console.log(`Server is running on port ${finalPort}`);
});

