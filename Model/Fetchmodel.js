const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
   
    productImg: {
        type: String,
        required: true,
    },
    productName: {
        type: String,
        required: true,
    },
    productPrice: {
        type: String, 
        required: true,
    },
    productRating:{
        type:Number,
        require:true
    }
});

const Productmodel = mongoose.model('Products', productSchema);

module.exports = Productmodel;