const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
   
    name: {
        type: String,
        required: true,
    },
    mobileNumber: {
        type: Number,
        required: true,
    },
   
    address: {
        type: String,
        required: true,
    },
    no_of_scoops:{
        type:Number,
        require:true
    }
});

const Ordermodel = mongoose.model('orderdetails', orderSchema);

module.exports = Ordermodel;