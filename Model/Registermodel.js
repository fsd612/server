const mongoose = require('mongoose');


    const registerSchema = new mongoose.Schema({
        Name: {
            type: String,
            required: true,
        },
        Email: {
            type: String,
            required: true,
        },
        Password: {
            type: String,
            required: true,
        },
        MobileNo:{
            type:Number,
            required:true,
        },
        DOB:{
            type:String,
            required:true,

        },
        Country:{
            type:String,
            required:true
        }
    });



const Registermodel =  mongoose.model('details', registerSchema);
module.exports = Registermodel;