
const express = require('express');
const Ordermodel = require('../Model/Ordermodel'); // Update the path as needed

let data = express.Router().post('/', async (req, res) => {
    try {
        const data = {
            "name" : req.body.name,
            "mobileNumber" : req.body.mobileNumber,
           "address" : req.body.address,
           "no_of_scoops" : parseInt(req.body.number_of_scoops)
        };

        // Create a new document using the Mongoose model
        const result = await Ordermodel.create(data);

        res.send("Data successfully inserted: " );
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
});

module.exports = data;

