
const express = require('express');
const router = express.Router();
const Loginmodel = require('../Model/Loginmodel') // Update the path as needed

router.post('/', async (req, res) => {
   const { Email, Password } = req.body;

  try {
      const user = await Loginmodel.findOne({ "Email": Email });

      if (user) {
         if (user.Password === Password) {
            res.send(user);
         } else {
            res.status(400).send("Password incorrect");
         }
      } else {
         res.status(400).send("User not found. Please register.");
      }
   } catch (error) {
      console.error(error);
      res.status(500).send("Internal Server Error");
   }
});

module.exports = router;