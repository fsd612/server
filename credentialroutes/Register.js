
const express = require('express');
const register = express.Router();
const Registermodel = require('../Model/Registermodel');

register.post('/', async (req, res) => {
   try {
      const emp = {
         "Name": req.body.Name,
         "Email": req.body.Email,
         "Password": req.body.Password,
         "MobileNo":req.body.MobileNo,
         "DOB" : req.body.DOB,
         "Country":req.body.Country
      };

      // Create a new document using the Mongoose model
      const result = await Registermodel.create(emp);

      res.send("Data inserted successfully" +result);
   } catch (error) {
      console.error(error);
      res.status(500).send("Internal Server Error");
   }
});

module.exports = register;

























