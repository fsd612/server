
const express = require('express');
const router = express.Router();
const ProductModel = require('../Model/Fetchmodel'); // Update the path as needed

const getData = router.get('/getProductData', async (req, res) => {
    try {
        
        const products = await ProductModel.find({});

        if (products.length > 0) {
            res.send(products);
        } else {
            res.send("No data found");
        }
    } catch (error) {
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
});

module.exports = getData;